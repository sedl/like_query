require_relative "lib/like_query/version"

Gem::Specification.new do |spec|
  spec.name = "like_query"
  spec.version = LikeQuery::VERSION
  spec.authors = ["christian"]
  spec.email = ["christian@sedlmair.ch"]
  spec.homepage = "https://gitlab.com/sedl/like_query"

  spec.summary = "helper for building active record calls."

  spec.description = "helper for building active record calls."

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the "allowed_push_host"
  # to allow pushing to a single host or delete this section to allow pushing to any host.

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/sedl/like_query"
  spec.metadata["changelog_uri"] = "https://gitlab.com/sedl/like_query"

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]
  end

  spec.add_dependency "rails", ">= 6.0.0"
  spec.add_runtime_dependency 'translighterate', ">= 0.3.0"

  spec.license = "MIT"
end