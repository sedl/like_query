module LikeQuery
  module ModelExtensions

    def like(search_string, schema = nil, debug: false, column_search: nil)

      _column_search = if [true, false].include?(column_search)
                         column_search
                       elsif [true, false].include?(Rails.configuration.x.like_query.column_search)
                         Rails.configuration.x.like_query.column_search
                       else
                         true
                       end

      search_string = '' if !search_string
      search_string = search_string.to_s unless search_string.is_a?(String)
      schema = [schema] if schema.is_a?(Symbol)
      @debug_like_query = debug

      raise 'like can only be called from a model' if self == ApplicationRecord
      if schema.first.is_a?(Array) && schema.length >= 2
        raise "only one array can be given: Either schema as one array or as multiple args, not as array"
      end

      queries = []
      associations = []
      available_column_keys = []
      @like_query_schema = schema

      search_string.split(' ').each do |s|

        f_str = format_string_element(s, _column_search)
        q = []

        (schema.first.is_a?(Array) ? schema.first : schema).each do |p|
          dbg("• Iterate schema => #{p}")
          enum_lab = enum_keys(p, s)

          if enum_lab.present?
            _q = arel_table[p].in(enum_lab).to_sql
            dbg(-> { "     Query from enum => #{_q}" })
            q.push(_q)

          elsif p.is_a?(Hash)
            p.each do |k, v|
              assoc = reflect_on_association(k)
              raise "Unknown association: :#{k}" unless assoc.present?
              associations.push(k.to_sym) unless associations.include?(k.to_sym)
              if v.is_a?(Symbol) || v.is_a?(String)
                available_column_keys.push([k.to_s, v.to_s].join('.'))
                q.push(query_string(v, f_str, assoc))
              elsif v.is_a?(Array)
                v.each do |_v|
                  available_column_keys.push([k.to_s, _v.to_s].join('.'))
                  q.push(query_string(_v, f_str, assoc))
                end
              else
                raise "unknown element: #{p}"
              end
            end

          else
            available_column_keys.push(p.to_s)
            q.push(query_string(p, f_str))

          end

        end

        # raise error
        if f_str[:column] && !available_column_keys.include?(f_str[:column])
          str = I18n.t(
            'like_query.column_not_available_error',
            default: 'column «%<column>s» is not present within available columns: «%<available_columns>s»'
          )
          raise format(str, column: f_str[:column], available_columns: available_column_keys.join(', '))
        end

        __q = q.compact
        queries.push("(#{__q.join(' OR ')})") if __q.present?
      end

      if associations.present?
        r = left_outer_joins(associations).where(queries.join(" AND "))
        dbg(-> { "RESULT => #{r.to_sql}" })
        r
      else
        r = where(queries.join(" AND "))
        dbg(-> { "RESULT => #{r.to_sql}" })
        r
      end

    end

    def generate_hash(output_schema = nil, limit: 50, image: nil, url: nil)
      c = LikeQuery::Collect.new(limit)
      c.collect(output_schema, limit: limit, image: image, url: url) { all }
      c.generate_hash
    end

    def generate_json(output_schema = nil, limit: 50, image: nil, url: nil)
      c = LikeQuery::Collect.new(limit)
      c.collect(output_schema, limit: limit, image: image, url: url) { all }
      c.generate_json
    end

    def like_query_schema
      @like_query_schema
    end

    private

    def enum_keys(column_name, value)
      en = defined_enums[column_name.to_s]
      return nil if en.nil?
      r = []
      en.each do |k, v|
        r.push(v) if k.downcase.include?(value.downcase)
      end
      r
    end

    def format_string_element(str, column_search)

      column = (column_search ? str.match(LikeQuery::Formatter::COLUMN_REGEX_CASE_INSENSITIVE) : nil)
      _str = (column ? str.match(/(?<=:)[\s\S]+/) : str).to_s
      match_number = _str.match(/^(\d+(?:\.\d+)?)$/)
      match_range = _str.match(/^(\d+(?:\.\d+)?)(\.\.)(\d+(?:\.\d+)?)$/)

      res = if match_range
              mr = match_range.to_s
              {
                float_from: mr.split('..').first.to_f,
                float_until: mr.split('..').last.to_f,
              }
            elsif match_number
              mn = match_number.to_s
              {
                float_from: mn,
                float_until: mn,
              }
            else
              {}
            end
      res[:string] = _str
      res[:column] = column.to_s if column
      res
    end

    def query_string(schema_column, search_string, association = nil)

      base_object = association&.klass || self
      begin
        type = base_object.columns_hash[schema_column.to_s].type
      rescue => e
        raise "#{base_object}.columns_hash[#{schema_column.to_s}].type RETURNED => «#{e.message}»"
      end
      arel = base_object.arel_table[schema_column]

      col = (association ? [association.name, schema_column.to_s].join('.') : schema_column.to_s)

      if !search_string[:column] || col == search_string[:column]
        if search_string[:float_from] && [:float, :decimal, :big_decimal, :integer, :bigint].include?(type)

          q = arel.between(search_string[:float_from]..search_string[:float_until]).to_sql
          dbg("     Query from number-field => #{q}")
          q

        else

          q = arel.matches("%#{search_string[:string]}%").to_sql
          dbg("     Query from string-field => #{q}")
          q

        end
      end
    end

    # def exception_on_unknown_column(search, schema)
    #   return unless search[:column]
    #
    #   keys = []
    #
    #   (schema.first.is_a?(Array) ? schema.first : schema).each do |p|
    #     if p.is_a?(Hash)
    #       p.each do |k, v|
    #         if v.is_a?(Array)
    #           else
    #         end
    #       end
    #     end
    #   end
    # end

    def dbg(label)

      return unless @debug_like_query

      if label.is_a?(Proc)
        lab = label.call
      else
        lab = label
      end
      Rails.logger.debug("LikeQuery: #{lab}")
    end
  end
end
