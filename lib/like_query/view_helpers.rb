module LikeQuery
  module ViewHelpers
    def like_query_mark(string, column = nil, find: @mark_string)
      res = string.to_s
      find.to_s.split(' ').each do |word|
        _col = word.match(LikeQuery::Formatter::COLUMN_REGEX_CASE_SENSITIVE)
        if !column || !_col || _col.to_s == column.to_s
          _word = (_col ? word.match(/(?<=:)[\s\S]+/) : word).to_s
          res = Translighterate.highlight(res, _word)
        end
      end
      res
    end
  end
end