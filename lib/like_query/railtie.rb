module LikeQuery
  class Railtie < Rails::Railtie
    config.after_initialize do
      #ActiveRecord::Base.send(:extend, LikeQuery::Extender)
      ActiveRecord::Base.send(:extend, LikeQuery::ModelExtensions)
    end
  end
end