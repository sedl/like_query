module LikeQuery
  class Collect
    def initialize(limit = nil)
      if limit
        @limit = limit
      elsif Rails.configuration.x.like_query.limit
        @limit = Rails.configuration.x.like_query.limit
      else
        @limit = nil
      end
      @length = 0
      @data = {}
      @overflow = false
      @columns_count = 0
      @sub_records_columns_count = 0
      @image = false
      @start_time = Time.now
      @schemes = {}
      @images = {}
      @urls = {}
      @no_action = {}
    end

    def set_schema(model, schema = nil, url: nil, no_action: false)
      @schemes[model.to_s] = schema_to_hash(schema)
      @urls[model.to_s] = url if url
      @no_action[model.to_s] = no_action
    end

    def collect(output_schema = nil, limit: nil, parent: nil, image: nil, url: nil, &block)

      _limit = (limit ? (@limit && @limit < limit ? @limit : limit) : @limit)
      return false if @length >= _limit
      length = 0

      recs = yield.includes(parent).limit(_limit)

      scm = (output_schema.present? ? output_schema : recs.like_query_schema)
      if scm.present?
        @schemes[recs.klass.to_s] = schema_to_hash(scm)
      end
      model_name = recs.klass.to_s
      schema = @schemes[model_name] || schema_to_hash(nil)

      _img = image || schema[:image]
      if _img.present?
        @images[model_name] = _img
        @image = true
      end

      _url = url || @urls[model_name]
      if url.present?
        @urls[model_name] = url
      end

      if parent
        parent_assoc = recs.klass.reflect_on_association(parent)
        if !parent_assoc
          raise "parent «#{parent}» is not a valid association"
        end
      end

      recs.each do |rec|

        if @length >= _limit
          @overflow = true
          break
        else

          if parent
            parent_record = rec.send(parent)
            r = record_to_hash(rec, schema, image, parent_record, url: _url)
            parent_class_name = parent_record.class.to_s
            parent_key = "#{parent_class_name}#{parent_record.id}"

            unless @data[parent_key]
              parent_schema = @schemes[parent_class_name]
              unless parent_schema
                Rails.logger.debug("WARNING: NO SCHEMA GIVEN FOR «#{parent_class_name}»")
                parent_schema = schema_to_hash(nil)
              end
              @data[parent_key] = record_to_hash(parent_record, parent_schema, @images[parent_class_name], url: @urls[parent_class_name])
              @length += 1
            end
            @data[parent_key][:children] ||= []
            @data[parent_key][:children].push(r)
          else
            r = record_to_hash(rec, schema, image, url: _url)
            @data["#{rec.class}#{rec.id}"] = r
          end
          c = (@image ? 1 : 0) + r[:values].to_a.length
          @columns_count = c if c > @columns_count
          @length += 1
          length += 1
        end
      end

      true
    end

    def generate_hash
      data = @data.map { |_, v| v }
      {
        data: data,
        length: @length,
        overflow: @overflow,
        columns_count: @columns_count,
        sub_records_columns_count: @sub_records_columns_count,
        image: @image,
        # sub_records_image: @sub_records_image,
        time: Time.now - @start_time
      }
    end

    def generate_json
      generate_hash.to_json
    end

    private

    def record_to_hash(record, schema, image, parent = nil, url: nil)
      r = {}
      schema[:values].each do |k|
        v = get_column_value(record, k)
        r[:values] ||= []
        r[:values].push(v)
        r[:attributes] ||= {}
        r[:attributes][k] = v
      end
      r[:id] = record.id
      if parent
        r[:model] = "#{parent.class.to_s}.#{record.class}"
        r[:parent_id] = parent.id
      else
        r[:model] = record.class.to_s
      end
      r[:image] = record.send(image) if image
      r[:url]  = url.yield(record) if url
      r[:no_action] = true if @no_action[record.class.to_s]
      r
    end

    def schema_to_hash(schema)

      if schema.is_a?(Array) && schema.first.is_a?(Array)
        _schema = schema.first
        raise 'invalid schema format' if schema.length >= 2
      else
        _schema = schema
      end

      if _schema.is_a?(String) || _schema.is_a?(Symbol)
        { values: [_schema.to_sym] }
      elsif _schema.is_a?(Array)
        r = {}
        _schema.each do |s|
          if s.is_a?(String) || s.is_a?(Symbol)
            r[:values] ||= []
            r[:values].push(s.to_sym)
          elsif s.is_a?(Hash)
            s.each do |k, v|
              if k.to_sym == :image
                r[:image] = v
              end
            end
          else
            raise "invalid schema format (#{s}) in schema => «#{schema}»"
          end
        end
        r
      elsif _schema.is_a?(Hash)
        _schema
      elsif !schema.present?
        { values: [] }
      else
        raise "invalid schema format => «#{schema}»"
      end
    end

    def get_column_value(record, column)
      val = nil
      if column.is_a?(Hash)
        r = []
        column.each do |k, v|
          if v.is_a?(Array)
            v.each do |_v|
              if _v.is_a?(String) || _v.is_a?(Symbol)
                r.push(get_column_value(record, "#{k}.#{_v}"))
              else
                raise "Too deeply nested objects: #{v}"
              end
            end
          elsif v.is_a?(String) || v.is_a?(Symbol)
            r.push(get_column_value(record, "#{k}.#{v}"))
          else
            raise "query column value can only be done by string or symbol, but given: #{v}"
          end
        end
        val = r.join(', ')
      else
        column.to_s.split('.').each { |i| val = (val ? val : record).send(i) }
      end
      (val ? val : '')
    end

  end
end