module LikeQuery
  module Extender
    def has_like_query
      extend LikeQuery::ModelExtensions
    end
  end
end